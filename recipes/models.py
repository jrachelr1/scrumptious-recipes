from pyexpat import model
from django.db import models


# Create your models here.
class Recipe(models.Model):
    name = models.CharField(max_length=125, default=None, blank=True, null=True)

    author = models.CharField(
        max_length=100, default=None, blank=True, null=True
    )
    description = models.TextField(
        "", max_length=125, default=None, blank=True, null=True
    )
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return self.name


class Measure(models.Model):
    name = models.CharField(
        max_length=30,
        unique=True,
        default=None,
        blank=True,
        null=True,
    )
    abbreviation = models.CharField(
        max_length=10, default=None, blank=True, null=True
    )

    def __str__(self):
        return self.name


class FoodItem(models.Model):
    name = models.CharField(
        max_length=100,
        default=None,
        blank=True,
        null=True,
    )

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    amount = models.FloatField()
    recipe = models.ForeignKey(
        "Recipe", related_name="Ingredients", on_delete=models.CASCADE
    )
    measure = models.ForeignKey(
        "Measure",
        related_name="Measure",
        on_delete=models.PROTECT,
        default=None,
        blank=True,
        null=True,
    )
    food = models.ForeignKey(
        "FoodItem",
        on_delete=models.PROTECT,
        default=None,
        blank=True,
        null=True,
    )

    def __str__(self):
        return self.name


class Step(models.Model):
    recipe = models.ForeignKey(
        "Step", related_name="Steps", on_delete=models.CASCADE
    )
    order = models.FloatField(default=None, blank=True, null=True)
    directions = models.CharField(
        max_length=300, default=None, blank=True, null=True
    )
    food_items = models.ManyToManyField("FoodItem")

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=20, default=None, blank=True, null=True)
    recipes = models.ManyToManyField("recipes.Recipe")

    def __str__(self):
        return self.name
